import {
  HOUR,
  MINUTE,
  DAY_OF_MONTH,
  MONTH,
  DAY_OF_WEEK,
} from "../const/time-unit";

export const isValidMinute = function (minute) {
  return minute >= MINUTE.MIN && minute <= MINUTE.MAX;
};
export const isValidHour = function (hour) {
  return hour >= HOUR.MIN && hour <= HOUR.MAX;
};
export const isValidDayOfMonth = function (day) {
  return day >= DAY_OF_MONTH.MIN && day <= DAY_OF_MONTH.MAX;
};
export const isValidMonth = function (month) {
  return month >= MONTH.MIN && month <= MONTH.MAX;
};
export const isValidDayOfWeek = function (number) {
  return number >= DAY_OF_WEEK.MIN && number <= DAY_OF_WEEK.MAX;
};

export const numberToDayOfWeek = function (number) {
  if (number >= DAY_OF_WEEK.MIN && number <= DAY_OF_WEEK.MAX && number) {
    switch (number) {
      case "0":
        return "Chủ nhật";
      default:
        return `Thứ ${parseInt(number) + 1}`;
    }
  } else return "";
};
export const formatTime = function (minute, hour) {
  let hourFormat = "";
  let minuteFormat = "";
  if (isValidHour(hour) && isValidMinute(minute)) {
    if (hour >= 0 && hour < 10) {
      hourFormat = hourFormat.concat("0").concat(hour);
    } else hourFormat = hourFormat.concat(hour);
    if (minute >= 0 && minute < 10) {
      minuteFormat = minuteFormat.concat("0").concat(minute);
    } else minuteFormat = minuteFormat.concat(minute);
    return hourFormat.concat(":").concat(minuteFormat);
  }
  return "";
};

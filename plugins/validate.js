import { extend } from "vee-validate";
import { required, length } from "vee-validate/dist/rules";

extend("minValue", (value) => {
  if (value <= 0) {
    return "Vui lòng nhập vào ô có giá trị lớn hơn 0";
  }
  return true;
});

extend("required", {
  ...required,
  message: "Lịch gửi bắt buộc",
});

extend("length", {
  ...length,
  message: "Lịch gửi bắt buộc",
});

extend("minLength", (value, [length]) => {
  console.log(value);
  console.log(value.length);
  if (value.length < length) {
    console.log("roi");
    return "Lịch gửi bắt buộc";
  }
  return true;
});

extend("checkboxRequired", (value, values) => {
  console.log(value);
  console.log(values);
  console.log(values.includes(value));
  for (let i = 0; i < value.length; i++) {
    if (value[i] > -1) {
      return true;
    }
  }
  return "Lịch gửi bắt buộc";
});

extend("minMinute", (value) => {
  if (value > 0) {
    return true;
  }
  return "Vui lòng nhập số phút lớn hơn 0";
});

extend("minHour", (value) => {
  if (value > 0) {
    return true;
  }
  return "Vui lòng nhập số giờ lớn hơn 0";
});

extend("minDay", (value) => {
  if (value > 0) {
    return true;
  }
  return "Vui lòng nhập số ngày lớn hơn 0";
});

extend("minMonth", (value) => {
  if (value > 0) {
    return true;
  }
  return "Vui lòng nhập số tháng lớn hơn 0";
});

extend("notEmpty", (value) => {
  if (value[0] !== "" && value[1] !== "") {
    return true;
  }
  return "Lịch gửi bắt buộc";
});

extend("max59", (value) => {
  if (value <= 59) {
    return true;
  }
  return "Vui lòng nhập số phút nhỏ hơn 60";
});

extend("max31", (value) => {
  if (value <= 31) {
    return true;
  }
  return "Vui lòng nhập số ngày nhỏ hơn 32";
});
